#!/usr/bin/env ruby

# Class for drivers
class Driver
  attr_reader :driver_name
  attr_reader :driver_x
  attr_reader :driver_y
  def initialize(driver_name, driver_x, driver_y)
    @driver_name = driver_name
    @driver_x = driver_x
    @driver_y = driver_y
  end
end

#Class for user
class User
  attr_reader :user_x
  attr_reader :user_y
  def initialize(user_x, user_y)
    @user_x = user_x
    @user_y = user_y
  end
end

# Generates a length x breadth map
# Map has indexes 0 to length-1 and 0 to breadth - 1 indexes
def generate_map(length, breadth)
  a = Array.new(breadth)
  a.map! { Array.new(length) }
  a
end

# Display the map and show where the user and drivers are
def show_map(map_array)
  map_array.each do |map_array_elements|
    map_array_elements.each do |elem|
      print '.' if elem.nil?
      print 'U' if elem.class == User
      print 'D' if elem.class == Driver
    end
    print "\n"
  end
end

def retrieve_drivers(map_array)
  array_of_drivers = []
  map_array.each do |map_array_elements|
    map_array_elements.each do |elem|
      array_of_drivers.push(elem) if elem.class == Driver
    end
  end
  array_of_drivers
end

def retrieve_user(map_array)
  map_array.each do |map_array_elements|
    map_array_elements.each do |elem|
      return elem if elem.class == User
    end
  end
end

def find_nearest_driver(xuser, yuser, map_array)
  available_drivers = retrieve_drivers(map_array)
  smallest_distance = 1.0 / 0
  nearest_driver_name = ''
  available_drivers.each do |each_driver|
    driver_x = each_driver.driver_x
    driver_y = each_driver.driver_y
    xselisih = xuser - driver_x
    yselisih = yuser - driver_y
    distance = xselisih.abs + yselisih.abs
    if distance < smallest_distance
      smallest_distance = distance
      nearest_driver_name = each_driver.driver_name
    end
  end
  "Driver: #{nearest_driver_name}"
end

def display_route(xdestination, ydestination, xuser, yuser)
  print_to_file = puts_to_file("start at (#{xuser},#{yuser})")
  print_to_file += puts_to_file("go to (#{xuser},#{ydestination})")
  print_to_file += if xuser > xdestination
                     puts_to_file(ydestination > yuser ? 'turn right' : 'turn left')
                   else
                     puts_to_file(ydestination > yuser ? 'turn left' : 'turn right')
                   end
  print_to_file += puts_to_file("go to (#{xdestination},#{ydestination})")
  print_to_file
end

def unit_cost
  600
end

def display_price_estimate(distance)
  price = distance * unit_cost
  puts_to_file("Price estimate: #{price}")
end

def ask_user_confirmation(print_to_file)
  puts 'Is this correct (Yes/No)'
  user_input = STDIN.gets.chomp
  if user_input == 'Yes'
    open('history.txt', 'a'){ |f|
      f.puts print_to_file + "\n\n"
    }
  end
end

def puts_to_file(word)
  puts word
  word + "\n"
end

def order_go_ride(map_array)
  print_to_file = ''
  puts 'User Destination in x,y?'
  user_destination = STDIN.gets.chomp
  user_destination = user_destination.split(',')
  xdestination = user_destination[0].to_i
  ydestination = user_destination[1].to_i
  user = retrieve_user(map_array)
  xselisih = user.user_x - xdestination
  yselisih = user.user_y - ydestination
  distance = xselisih.abs + yselisih.abs
  if distance > 0
    print_to_file += puts_to_file(
      find_nearest_driver(
        user_destination[0].to_i, user_destination[1].to_i, map_array
      )
    )
    print_to_file += puts_to_file("Route: ")
    route = display_route(
        user_destination[0].to_i, user_destination[1].to_i,
        user.user_x, user.user_y
      )
    print_to_file += route
    print_to_file += display_price_estimate(distance)
    ask_user_confirmation(print_to_file)
  else
    puts "Cannot go to the same destination as where the user is right now"
  end
end

def read_file(filename)
  data = ''
  f = File.open(filename, 'r')
  f.each_line do |line|
    data += line
  end
  data
end

# Choice of Actions
def functions(map_array)
  while(true)
  puts "Choices of action
    Show Map
    Order Go Ride
    View History
To execute an action simply type the action that you want the program to perform.
e.g. I want the program to execute the action Show Map.
I simply type \"Show Map\" (without parantheses) in the terminal"
    user_input_function = STDIN.gets.chomp
    show_map(map_array) if user_input_function == 'Show Map'
    if user_input_function == 'Order Go Ride'
      order_go_ride(map_array)
      exit
    end
    puts read_file('history.txt') if user_input_function == 'View History'
    exit if user_input_function == 'exit'
  end
end

#Go Client
def go_cli(*args)
  a = generate_map(0,0)
  if args.length == 0
    driver_x_array = *(0..20)
    driver_y_array = *(0..20)
    xuser = driver_x_array.sample
    yuser = driver_y_array.sample
    a = generate_map(20, 20)
    a[yuser][xuser] = User.new(yuser, xuser)
    driver_x_array.delete(xuser)
    driver_y_array.delete(yuser)
    number_of_drivers = 5
    (1..number_of_drivers).each do |number|
      driver_x = driver_x_array.sample
      driver_y = driver_y_array.sample
      driver_name = "Driver #{number.to_i}"
      a[driver_y][driver_x] = Driver.new(driver_name, driver_x, driver_y)
      driver_x_array.delete(xuser)
      driver_y_array.delete(yuser)
    end
  end
  if args.length == 3
    xuser = args[1].to_i
    yuser = args[2].to_i
    size = args[0].to_i
    driver_x_array = *(0..size)
    driver_y_array = *(0..size)
    a = generate_map(size, size)
    driver_x_array.delete(xuser)
    driver_y_array.delete(yuser)
    a[yuser][xuser] = User.new(yuser, xuser)
    number_of_drivers = 5
    (1..number_of_drivers).each do |number|
      driver_x = driver_x_array.sample
      driver_y = driver_y_array.sample
      driver_name = "Driver #{number.to_i}"
      a[driver_y][driver_x] = Driver.new(driver_name, driver_x, driver_y)
      driver_x_array.delete(xuser)
      driver_y_array.delete(yuser)
    end
  end
  if args.length == 6
    length = args[0].to_i
    width = args[1].to_i
    xuser = args[2].to_i
    yuser = args[3].to_i
    driver_x_array = *(0..width)
    driver_y_array = *(0..length)
    a = generate_map(length, width)
    driver_x_array.delete(xuser)
    driver_y_array.delete(yuser)
    a[yuser][xuser] = User.new(yuser, xuser)
    number_of_drivers = args[4].to_i
    (0...number_of_drivers).each do |number|
      driver_x = args[5][number][1].to_i
      driver_y = args[5][number][2].to_i
      driver_name = args[5][number][0]
      a[driver_y][driver_x] = Driver.new(driver_name, driver_x, driver_y)
      driver_x_array.delete(xuser)
      driver_y_array.delete(yuser)
    end
  end
  functions(a)
end

#Reads the input file which follows the format in readme.md
def read_input_file(filename)
  data = ''
  f = File.open(filename, 'r')
  lines_read = 1
  array_params = []
  array_drivers_data = []
  f.each_line do |line|
    line = line.chomp
    if lines_read > 3
      array_drivers_data.push(line.split(' '))
    else
      array_params.push(line.split(' '))
    end
    lines_read += 1
  end
  [array_params, array_drivers_data]
end

argv_input = ARGV
if argv_input.length == 3
  length_and_breadth = argv_input[0]
  xuser = argv_input[1]
  yuser = argv_input[2]
  go_cli(length_and_breadth, xuser, yuser)
elsif argv_input.length == 1
  array_file_read = read_input_file(argv_input[0])
  length = array_file_read[0][0][0]
  breadth =  array_file_read[0][0][1]
  xuser = array_file_read[0][1][0]
  yuser = array_file_read[0][1][1]
  number_of_drivers = array_file_read[0][2][0]
  array_drivers_data = array_file_read[1]
  go_cli(length, breadth, xuser, yuser, number_of_drivers, array_drivers_data)
else
  go_cli
end
