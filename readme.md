# go-cli
go-cli is a command line app made according to the specifications inside the file below

https://drive.google.com/file/d/1BC_tfgekX2pHJ7nWGbXguhTS8-NUlEQH/view

While making this program, the author found some things which are not clearly defined in the file above. Because of this, these assumptions were made while he wrote the app. 
  - User destination coordinates are always inside the map.
  - The number of input parameters is always either 3, 1, 0 and no matter how many they are it is always correct.
  - The user has added a link to the file inside the directory /user/local/bin/ or has placed the app inside that particular directory.
  - Input file and Output file follows the format which will be elaborated below
  - Program exits after the action Order Go Ride is performed

## Input File Format
The input file format will be as follows:

(length_of_map),(width_of_map)
(user_x),(user_y)
(number_of_drivers)
(Name_of_driver),(driver_x),(driver_y)

The last line is repeated by how many drivers there are (indicated by number_of_drivers).

This format is chosen to ease the author of the burden of having to write the extra codes needed to retrieve the data from a differently formatted input file.

## Output File Format
The output file format will be as follows:

Driver: (Driver_name)
Route:
(route)
Price estimate: (Price_estimate)

Every subsequent entries will be separated by one line.

This format is chosen for its readability.

